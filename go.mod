module gitlab.com/telecom-tower/grpc-renderer

require (
	github.com/konsorten/go-windows-terminal-sequences v1.0.1 // indirect
	github.com/pkg/errors v0.8.0
	github.com/sirupsen/logrus v1.1.1
	gitlab.com/telecom-tower/sdk v1.0.2
	gitlab.com/telecom-tower/towerapi v1.0.1
	golang.org/x/crypto v0.0.0-20181015023909-0c41d7ab0a0e // indirect
	golang.org/x/image v0.0.0-20180926015637-991ec62608f3
	golang.org/x/net v0.0.0-20181011144130-49bb7cea24b1
	google.golang.org/grpc v1.15.0
)
