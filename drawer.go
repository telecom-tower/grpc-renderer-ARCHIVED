// Copyright 2018 Jacques Supcik / HEIA-FR
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// This package implements the link between a gRPC server and a ws281x "neopixel"
// device. It is a part of the telecom tower project

//go:generate protoc -I ../towerapi/v1 telecomtower.proto --go_out=plugins=grpc:../towerapi/v1

package renderer

import (
	"fmt"
	"image"
	"image/color"
	"io"
	"strings"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/telecom-tower/grpc-renderer/font"
	"gitlab.com/telecom-tower/sdk"
	pb "gitlab.com/telecom-tower/towerapi/v1"
	"golang.org/x/image/colornames"
	"golang.org/x/net/html"
)

func resetLayer(l *layer) {
	l.image = image.NewRGBA(image.Rect(0, 0, 0, 0))
	l.origin = image.Point{0, 0}
	l.dirty = true
	l.alpha = 0xffff
	l.rolling.mode = sdk.RollingStop
	l.rolling.entry = 0
	l.rolling.separator = 0
}

func (tower *TowerRenderer) init(clear *pb.Init) error {
	log.Debugf("init")
	for l := 0; l < maxLayers; l++ {
		resetLayer(tower.layers[l])
		tower.activeLayers[l] = false
	}
	return nil
}

func (tower *TowerRenderer) clear(clear *pb.Clear) error {
	log.Debugf("clear")
	for _, l := range clear.Layer {
		resetLayer(tower.layers[l])
		tower.activeLayers[l] = false
	}
	return nil
}

func (tower *TowerRenderer) setPixels(pixels *pb.SetPixels) error {
	log.Debugf("set pixels")
	tower.activeLayers[pixels.Layer] = true
	layer := tower.layers[pixels.Layer]
	canvas := layer.image
	for _, pix := range pixels.Pixels {
		point := image.Point{
			X: int(pix.Point.X),
			Y: int(pix.Point.Y),
		}
		canvas = resizeImage(
			canvas,
			image.Rect(point.X, point.Y, point.X+1, point.Y+1))
		paint(canvas, point.X, point.Y, pbColorToColor(pix.Color), int(pixels.PaintMode))
	}
	tower.layers[pixels.Layer].image = canvas
	return nil
}

func (tower *TowerRenderer) drawRectangle(rect *pb.DrawRectangle) error {
	log.Debug("draw rectangle")
	tower.activeLayers[rect.Layer] = true
	layer := tower.layers[rect.Layer]
	layer.dirty = true
	canvas := layer.image
	c := pbColorToColor(rect.Color)
	r := image.Rect(int(rect.Min.X), int(rect.Min.Y), int(rect.Max.X), int(rect.Max.Y))
	canvas = resizeImage(canvas, r)
	for x := r.Min.X; x < r.Max.X; x++ {
		for y := r.Min.Y; y < r.Max.Y; y++ {
			paint(canvas, x, y, c, int(rect.PaintMode))
		}
	}
	tower.layers[rect.Layer].image = canvas
	return nil
}

func (tower *TowerRenderer) drawBitmap(bitmap *pb.DrawBitmap) error {
	log.Debug("draw bitmap")
	bounds := image.Rect(
		int(bitmap.Position.X),
		int(bitmap.Position.Y),
		int(bitmap.Position.X)+int(bitmap.Width),
		int(bitmap.Position.Y)+int(bitmap.Height),
	)
	tower.activeLayers[bitmap.Layer] = true
	layer := tower.layers[bitmap.Layer]
	layer.dirty = true
	canvas := layer.image
	canvas = resizeImage(canvas, bounds)
	i := 0
	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		for x := bounds.Min.X; x < bounds.Max.X; x++ {
			paint(canvas, x, y, pbColorToColor(bitmap.Colors[i]), int(bitmap.PaintMode))
			i++
		}
	}
	tower.layers[bitmap.Layer].image = canvas
	return nil
}

func (tower *TowerRenderer) writeTextAt(txt string, fnt *font.Font, pos int, color color.Color,
	paintMode int, canvas *image.RGBA) (canvasRes *image.RGBA, width int, err error) {

	log.Debugf("Writing \"%s\" at %d with font size %d", txt, pos, fnt.Width)
	msg, err := font.ExpandAlias(txt)
	if err != nil {
		err = errors.WithMessage(err, "Error expanding text")
		log.Debug(err)
		return nil, 0, err
	}

	textLen := 0
	for _, r := range msg {
		if _, ok := fnt.Bitmap[r]; ok {
			textLen++
		}
	}

	width = fnt.Width * textLen
	rect := image.Rect(pos, 0, pos+width, 8)
	canvas = resizeImage(canvas, rect)
	for _, r := range msg {
		if bmap, ok := fnt.Bitmap[r]; ok {
			for _, glyph := range bmap {
				for y := 0; y < 8; y++ {
					if uint(glyph)&(1<<uint(y)) != 0 {
						paint(canvas, pos, y, color, paintMode)
					}
				}
				pos++
			}
		}
	}
	return canvas, width, nil
}

func thisFont(name string) (*font.Font, error) {
	if strings.HasPrefix(name, "8") {
		return &font.Font8x8, nil
	} else if strings.HasPrefix(name, "6") {
		return &font.Font6x8, nil
	} else {
		err := errors.New("Unknown font")
		log.Debug(err)
		return nil, err
	}
}

func hexColor(scol string) (color.Color, error) {
	var format string
	var factor uint8

	if len(scol) == 7 {
		format = "#%02x%02x%02x"
		factor = 1
	} else if len(scol) == 4 {
		format = "#%1x%1x%1x"
		factor = 17
	} else {
		err := errors.New("Invalid color")
		log.Debug(err)
		return color.RGBA{0, 0, 0, 255}, err
	}
	var r, g, b uint8
	n, err := fmt.Sscanf(scol, format, &r, &g, &b)
	if err != nil {
		return color.RGBA{0, 0, 0, 255}, err
	}
	if n != 3 {
		err := errors.Errorf("color: %v is not a hex-color", scol)
		log.Debug(err)
		return color.RGBA{0, 0, 0, 255}, err
	}
	return color.RGBA{r * factor, g * factor, b * factor, 255}, nil
}

func thisColor(name string) (color.Color, error) {
	if strings.HasPrefix(name, "#") {
		return hexColor(name)
	}
	c, ok := colornames.Map[name]
	if ok {
		return c, nil
	}
	err := errors.New("color not found")
	log.Debug(err)
	return colornames.Black, err
}

// nolint: gocyclo
func (tower *TowerRenderer) writeHTMLTextAt(txt string, fnt *font.Font, pos int, col color.Color,
	paintMode int, canvas *image.RGBA) (*image.RGBA, error) {

	log.Debug("Rendering HTML")
	type style struct {
		fnt   *font.Font
		color color.Color
	}

	stack := make([]style, 0)
	z := html.NewTokenizer(strings.NewReader(txt))

	for {
		tt := z.Next()
		switch tt {
		case html.ErrorToken:
			err := z.Err()
			if err == io.EOF {
				err = nil
			} else {
				log.Debug(err)
			}
			return canvas, err
		case html.TextToken:
			var err error
			var length int
			canvas, length, err = tower.writeTextAt(string(z.Text()), fnt, pos, col, paintMode, canvas)
			if err != nil {
				return canvas, err
			}
			pos += length
		case html.StartTagToken:
			tn, hasAttr := z.TagName()
			switch string(tn) {
			case "text":
				// ignore
			case "font":
				stack = append(stack, style{fnt, col})
				for hasAttr {
					var av, ak []byte
					ak, av, hasAttr = z.TagAttr()
					switch string(ak) {
					case "size":
						f, err := thisFont(string(av))
						if err == nil {
							fnt = f
						} // ignore attr if font not found
					case "color":
						c, err := thisColor(string(av))
						if err == nil {
							col = c
						} // ignore attr if color not found
					}
				}
			case "b":
				stack = append(stack, style{fnt, col})
				fnt, _ = thisFont("8")
			case "i":
				stack = append(stack, style{fnt, col})
				fnt, _ = thisFont("6")
			}

		case html.EndTagToken:
			tn, _ := z.TagName()
			switch string(tn) {
			case "text":
				// ignore
			case "font", "b", "i":
				if len(stack) > 0 {
					fnt = stack[len(stack)-1].fnt
					col = stack[len(stack)-1].color
					stack = stack[0 : len(stack)-1]
				}
			}
		}
	}
}

func (tower *TowerRenderer) writeText(wt *pb.WriteText) error {
	log.Debug("write text")
	tower.activeLayers[wt.Layer] = true
	layer := tower.layers[wt.Layer]
	layer.dirty = true
	canvas := layer.image

	fnt, err := thisFont(wt.Font)
	if err != nil {
		log.Debug(err)
		return err
	}

	txt := wt.Text
	pos := int(wt.X)
	col := pbColorToColor(wt.Color)
	paintMode := int(wt.PaintMode)

	if strings.HasPrefix(txt, "<text>") && strings.HasSuffix(txt, "</text>") {
		canvas, err = tower.writeHTMLTextAt(txt, fnt, pos, col, paintMode, canvas)
	} else {
		canvas, _, err = tower.writeTextAt(txt, fnt, pos, col, paintMode, canvas)
	}
	if err == nil {
		tower.layers[wt.Layer].image = canvas
	} else {
		log.Debug(err)
	}
	return err
}

func (tower *TowerRenderer) setLayerOrigin(origin *pb.SetLayerOrigin) error {
	log.Debug("Set Layer Origin")
	tower.activeLayers[origin.Layer] = true
	layer := tower.layers[origin.Layer]
	layer.dirty = true
	layer.origin = image.Point{X: int(origin.Position.X), Y: int(origin.Position.Y)}
	layer.image = resizeImage(
		layer.image,
		image.Rect(
			layer.origin.X,
			layer.origin.Y,
			layer.origin.X+displayWidth,
			layer.origin.Y+displayHeight))
	return nil
}

func (tower *TowerRenderer) setLayerAlpha(alpha *pb.SetLayerAlpha) error {
	log.Debug("Set Layer Alpha")
	tower.activeLayers[alpha.Layer] = true
	layer := tower.layers[alpha.Layer]
	layer.dirty = true
	layer.alpha = int(alpha.Alpha)
	return nil
}

func (tower *TowerRenderer) autoRoll(autoroll *pb.AutoRoll) error {
	log.Debugf("AutoRoll (%v)", autoroll.Mode)
	tower.activeLayers[autoroll.Layer] = true
	layer := tower.layers[autoroll.Layer]
	layer.dirty = true
	layer.rolling.mode = int(autoroll.Mode)
	layer.rolling.entry = int(autoroll.Entry)
	layer.rolling.separator = int(autoroll.Separator)
	return nil
}

// Draw implements the main task of the server, namely drawing on the display
func (tower *TowerRenderer) Draw(stream pb.TowerDisplay_DrawServer) error { // nolint: gocyclo
	var status error
	for i := 0; i < maxLayers; i++ {
		tower.layers[i].dirty = false
	}
	for {
		in, err := stream.Recv()
		if err == io.EOF {
			if status == nil {
				tower.lsc <- tower.getLayersSet()
			}
			msg := ""
			if status != nil {
				msg = status.Error()
			}
			return stream.SendAndClose(&pb.DrawResponse{
				Message: msg,
			})
		}
		if err != nil {
			return err
		}

		if status == nil {
			switch t := in.Type.(type) {
			case *pb.DrawRequest_Init:
				status = tower.init(t.Init)
			case *pb.DrawRequest_Clear:
				status = tower.clear(t.Clear)
			case *pb.DrawRequest_SetPixels:
				status = tower.setPixels(t.SetPixels)
			case *pb.DrawRequest_DrawRectangle:
				status = tower.drawRectangle(t.DrawRectangle)
			case *pb.DrawRequest_DrawBitmap:
				status = tower.drawBitmap(t.DrawBitmap)
			case *pb.DrawRequest_WriteText:
				status = tower.writeText(t.WriteText)
			case *pb.DrawRequest_SetLayerOrigin:
				status = tower.setLayerOrigin(t.SetLayerOrigin)
			case *pb.DrawRequest_SetLayerAlpha:
				status = tower.setLayerAlpha(t.SetLayerAlpha)
			case *pb.DrawRequest_AutoRoll:
				status = tower.autoRoll(t.AutoRoll)
			}
		}
	}
}
